package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	@Test
	public void scoreShouldBeIncreased() {
		//Arrange
		Player player = new Player("Federer", 0);
		//Act
		player.incrementScore();
		//Assert
		assertEquals(1,player.getScore());
	}
	
	@Test
	public void scoreShouldNotBeIncreased() {
		//Arrange
		Player player = new Player("Federer", 0);
		//Assert
		assertEquals(0,player.getScore());
	}
	
	@Test
	public void scoreShouldBeLove() {
		//Arrange
		Player player = new Player("Federer", 0);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("love", scoreAsString);
	}
	
	@Test
	public void scoreShouldBeFifteen() {
		//Arrange
		Player player = new Player("Federer", 1);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("fifteen", scoreAsString);
	}
	
	@Test
	public void scoreShouldBeThirty() {
		//Arrange
		Player player = new Player("Federer", 2);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("thirty", scoreAsString);
	}
	
	@Test
	public void scoreShouldBeForty() {
		//Arrange
		Player player = new Player("Federer", 3);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertEquals("forty", scoreAsString);
	}
	
	@Test
	public void scoreShouldBeNullNegative() {
		//Arrange
		Player player = new Player("Federer", -1);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertNull(scoreAsString);
	}
	
	@Test
	public void scoreShouldBeNullIfMoreThanThree() {
		//Arrange
		Player player = new Player("Federer", 4);
		//Act
		String scoreAsString = player.getScoreAsString();
		//Assert
		assertNull(scoreAsString);
	}
	
	@Test
	public void shouldBeTie() {
		//Arrange
		Player player1 = new Player("Federer", 2);
		Player player2 = new Player("Nadal", 2);
		//Act
		boolean tie = player1.isTieWith(player2);
		//Assert
		assertTrue(tie);
	}
	
	@Test
	public void shouldBeNotTie() {
		//Arrange
		Player player1 = new Player("Federer", 1);
		Player player2 = new Player("Nadal", 2);
		//Act
		boolean tie = player1.isTieWith(player2);
		//Assert
		assertFalse(tie);
	}
	
	@Test
	public void shouldHaveAtLeastFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 3);
		//Act
		boolean outcome = player1.hasAtLeastFortyPoints();
		//Assert
		assertTrue(outcome);
	}
	
	@Test
	public void shouldNotHaveAtLeastFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 2);
		//Act
		boolean outcome = player1.hasAtLeastFortyPoints();
		//Assert
		assertFalse(outcome);
	}
	
	@Test
	public void shouldHaveLessThanFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 2);
		//Act
		boolean outcome = player1.hasLessThanFortyPoints();
		//Assert
		assertTrue(outcome);
	}
	
	@Test
	public void shouldNotHaveLessThanFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 3);
		//Act
		boolean outcome = player1.hasLessThanFortyPoints();
		//Assert
		assertFalse(outcome);
	}
	
	@Test
	public void shouldHaveMoreThanFourtyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 4);
		//Act
		boolean outcome = player1.hasMoreThanFourtyPoints();
		//Assert
		assertTrue(outcome);
	}
	
	@Test
	public void shouldNotHaveMoreThanFourtyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 3);
		//Act
		boolean outcome = player1.hasMoreThanFourtyPoints();
		//Assert
		assertFalse(outcome);
	}
	
	@Test
	public void shouldHaveOnePointAdvantageOn() {
		//Arrange
		Player player1 = new Player("Federer", 3);
		Player player2 = new Player("Nadal", 2);
		//Act
		boolean tie = player1.hasOnePointAdvantageOn(player2);
		//Assert
		assertTrue(tie);
	}
	
	@Test
	public void shouldNotHaveOnePointAdvantageOn() {
		//Arrange
		Player player1 = new Player("Federer", 3);
		Player player2 = new Player("Nadal", 3);
		//Act
		boolean tie = player1.hasOnePointAdvantageOn(player2);
		//Assert
		assertFalse(tie);
	}
	
	@Test
	public void shouldHaveAtLeastTwoPointsAdvantageOn() {
		//Arrange
		Player player1 = new Player("Federer", 5);
		Player player2 = new Player("Nadal", 3);
		//Act
		boolean tie = player1.hasAtLeastTwoPointsAdvantageOn(player2);
		//Assert
		assertTrue(tie);
	}
	
	@Test
	public void shouldNotHaveAtLeastTwoPointsAdvantageOn() {
		//Arrange
		Player player1 = new Player("Federer", 4);
		Player player2 = new Player("Nadal", 3);
		//Act
		boolean tie = player1.hasAtLeastTwoPointsAdvantageOn(player2);
		//Assert
		assertFalse(tie);
	}
	

	
	
	
	
	

}
