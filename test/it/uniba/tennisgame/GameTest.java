package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void shouldScoreSituation12() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = "Federer";
		String playerName2 = "Nadal";
		//Act
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		String status = game.getGameStatus();
		//Assert
		assertEquals("Federer fifteen - Nadal thirty",status);
	}
	
	@Test
	public void shouldScoreSituation32() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = "Federer";
		String playerName2 = "Nadal";
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		String status = game.getGameStatus();
		//Assert
		assertEquals("Federer forty - Nadal thirty",status);
		
	}
	
	@Test
	public void shouldScoreSituation42() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = "Federer";
		String playerName2 = "Nadal";
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		
		String status = game.getGameStatus();
		//Assert
		assertEquals("Federer wins",status);
		
	}
	
	@Test
	public void shouldScoreSituation13() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = "Federer";
		String playerName2 = "Nadal";
		//Act
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		String status = game.getGameStatus();
		//Assert
		assertEquals("Federer fifteen - Nadal forty",status);
		
	}
	
	@Test
	public void shouldScoreSituation33() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = "Federer";
		String playerName2 = "Nadal";
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		String status = game.getGameStatus();
		//Assert
		assertEquals("Deuce",status);
		
	}
	
	@Test
	public void shouldScoreSituation43() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = "Federer";
		String playerName2 = "Nadal";
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		
		String status = game.getGameStatus();
		//Assert
		assertEquals("Advantage Federer",status);
		
	}
	
	@Test
	public void shouldScoreSituation14() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = "Federer";
		String playerName2 = "Nadal";
		//Act
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		String status = game.getGameStatus();
		//Assert
		assertEquals("Nadal wins",status);
		
	}
	
	@Test
	public void shouldScoreSituation34() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = "Federer";
		String playerName2 = "Nadal";
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		String status = game.getGameStatus();
		//Assert
		assertEquals("Advantage Nadal",status);
		
	}
	
	@Test
	public void shouldScoreSituation44() throws Exception {
		//Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = "Federer";
		String playerName2 = "Nadal";
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		String status = game.getGameStatus();
		//Assert
		assertEquals("Deuce",status);
		
	}
	
	
}
